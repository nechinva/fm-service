<?php
/**
 * Api endpoint
 */

require_once 'includes/db.class.php';
require_once 'includes/api.class.php';
require_once 'includes/entity.class.php';

try {
    $api = new EntityApi();
    echo $api->run();
} catch (Exception $e) {
    echo json_encode(['error' => $e->getMessage()]);
}