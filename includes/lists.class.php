<?php

/**
 * Class ListsApi
 */
class ListsApi extends EntityApi
{
    /**
     * Get all lists
     *
     * URI: [domain]/api/v1/lists/index/
     * Method: GET
     *
     * @return false|string
     * @throws Exception
     */
    public function indexAction()
    {
        $lists = (new DB())->query('SELECT `id`, `name` FROM `lists` WHERE `active` = 1')->fetch();
        if ($lists) {
            return $this->response(['data' => $lists], 200);
        }

        return $this->response(['error' => 'Data not found'], 404);
    }

    /**
     * Get list with id
     *
     * URI: [domain]/api/v1/lists/view/{listId}/
     * Method: GET
     *
     * @return false|string
     * @throws Exception
     */
    public function viewAction()
    {
        $listId = $this->getId();
        if ($listId) {
            $list = (new DB())->query('SELECT `id`, `name` FROM `lists` WHERE `id` = ? AND `active` = 1', $listId)->fetch();
            if ($list) {
                return $this->response(['data' => $list], 200);
            }
        }

        return $this->response(['error' => 'Data not found'], 404);
    }

    /**
     * Create new list
     *
     * URI: [domain]/api/v1/lists/create/?name=list-name
     * Method: POST
     *
     * @return false|string
     * @throws Exception
     */
    public function createAction()
    {
        $listName = $this->getParameter('name', '');
        if ('' !== trim($listName)) {
            $insert = (new Db())->query('INSERT INTO `lists` (`name`) VALUES (?)', trim($listName));
            if ($insert->affectedRows()) {
                return $this->response(['data' => $insert->affectedRows()], 200);
            }
        }

        return $this->response(['error' => 'Saving error'], 500);
    }

    /**
     * Update the list
     *
     * URI: [domain]/api/v1/lists/update/{listId}/?name=new-list-name
     * Method: PUT
     *
     * @return false|string
     * @throws Exception
     */
    public function updateAction()
    {
        $listId   = $this->getId();
        $listName = $this->getParameter('name', '');
        if ($listId && '' !== $listName) {
            $update = (new Db())->query('UPDATE `lists` SET `name` = ? WHERE `id` = ?', trim($listName), $listId);
            if ($update->affectedRows()) {
                return $this->response(['data' => $update->affectedRows()], 200);
            }
        }

        return $this->response(['error' => 'Updating error'], 400);
    }

    /**
     * Delete the list
     *
     * URI: [domain]/api/v1/lists/delete/{listId}/
     * Method: DELETE
     *
     * @return false|string
     * @throws Exception
     */
    public function deleteAction()
    {
        $listId = $this->getId();
        if ($listId) {
            $delete = (new Db())->query('UPDATE `lists` SET `active` = 0 WHERE `id` = ?', $listId);
            if ($delete->affectedRows()) {
                return $this->response(['data' => $delete->affectedRows()], 200);
            }
        }

        return $this->response(['error' => 'Deleting error'], 500);
    }
}
