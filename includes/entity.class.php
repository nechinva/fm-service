<?php

/**
 * Class EntityApi
 */
class EntityApi extends Api
{
    /**
     * EntityApi constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Check the authentication header token
     *
     * @param $token
     *
     * @return bool
     * @throws Exception
     */
    private function checkAuthHeader($token)
    {
        $user = (new DB())->query('SELECT * FROM `users` WHERE `token` = ? AND `active` = 1', $token)->fetch('array');
        return isset($user['id']) && $user['id'];
    }

    /**
     * Check the requested method
     *
     * @return bool
     */
    private function checkMethod()
    {
        switch ($this->method) {
            case 'GET':
                return 'view' === $this->action || 'index' === $this->action;
            case 'POST':
                return 'create' === $this->action;
            case 'PUT':
                return 'update' === $this->action;
            case 'DELETE':
                return 'delete' === $this->action;
            default:
                return false;
        }
    }

    /**
     * Check the requested parameters
     */
    protected function check()
    {
        // Check the auth headers
        $headers = $this->getAllHeaders();

        // hash_hmac('haval160,4', 'admin@localhost.ru', 'fm-secret-test')
        if (!isset($headers['auth-token']) || !$this->checkAuthHeader($headers['auth-token'])) {
            throw new RuntimeException('Authentication error', 404);
        }

        // The first 2 elements of the URI array must be "api" and the name of the entity
        if ('api' !== $this->parsedUri['api'] || !file_exists(__DIR__ . '/' . $this->parsedUri['entity'] . '.class.php')) {
            throw new RuntimeException('API not found', 404);
        }

        // Check the Api version
        if ($this->parsedUri['version'] !== $this->apiVersion) {
            throw new RuntimeException('This API version is not supported', 404);
        }

        // Check the current action
        $this->action = $this->parsedUri['action'];
        if (!in_array($this->action, $this->availableActions)) {
            throw new RuntimeException('This action is not available', 404);
        }

        // Check the current method
        if (!$this->checkMethod()) {
            throw new RuntimeException('The requested method for this action is not available', 404);
        }
    }

    /**
     * Execution the Api
     *
     * @return mixed
     */
    public function run()
    {
        $this->check();

        require_once $this->parsedUri['entity'] . '.class.php';

        $entityName = ucfirst($this->parsedUri['entity']) . 'Api';
        $entity = new $entityName();

        $action = $this->action . 'Action';
        if (method_exists($entity, $action)) {
            return $entity->{$action}();
        } else {
            throw new RuntimeException('Invalid method', 405);
        }
    }

    protected function indexAction() {}

    protected function viewAction() {}

    protected function createAction() {}

    protected function updateAction() {}

    protected function deleteAction() {}
}