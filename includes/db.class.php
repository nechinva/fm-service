<?php

/**
 * Database class
 *
 * Class DB
 */
class DB
{
    /**
     * @var string - Configuration parameters
     */
    private $host = 'localhost';
    private $user = 'root';
    private $pass = '';
    private $name = 'fm-db';
    private $charset = 'utf8';

    /**
     * @var mysqli - Database connection
     */
    protected $connection;

    /**
     * @var - Database query
     */
    protected $query;

    /**
     * @var int - Database query count
     */
    public $queryCount = 0;

    /**
     * DB constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        $this->connection = new mysqli(
            $this->host, $this->user, $this->pass, $this->name
        );
        if ($this->connection->connect_error) {
            throw new Exception(
                'Failed to connect to MySQL - '
                . $this->connection->connect_error
            );
        }
        $this->connection->set_charset($this->charset);
    }

    /**
     * Execute the database query
     *
     * @param $query
     *
     * @return $this
     * @throws Exception
     */
    public function query($query)
    {
        $this->query = $this->connection->prepare($query);
        if ($this->query) {
            if (func_num_args() > 1) {
                $x       = func_get_args();
                $args    = array_slice($x, 1);
                $types   = '';
                $argsRef = [];
                foreach ($args as $k => &$arg) {
                    if (is_array($args[$k])) {
                        foreach ($args[$k] as $j => &$a) {
                            $types     .= $this->_getType($args[$k][$j]);
                            $argsRef[] = &$a;
                        }
                    } else {
                        $types     .= $this->_getType($args[$k]);
                        $argsRef[] = &$arg;
                    }
                }
                array_unshift($argsRef, $types);
                call_user_func_array([$this->query, 'bind_param'], $argsRef);
            }
            $this->query->execute();
            if ($this->query->errno) {
                throw new Exception(
                    'Unable to process MySQL query (check your params) - '
                    . $this->query->error
                );
            }
            $this->queryCount++;
        } else {
            throw new Exception(
                'Unable to prepare statement (check your syntax) - '
                . $this->connection->error
            );
        }
        return $this;
    }

    /**
     * Fetch the database request
     *
     * @param string $type - Result type (all|array)
     *
     * @return array
     */
    public function fetch($type = 'all')
    {
        $params = [];
        $row    = [];
        $meta   = $this->query->result_metadata();
        while ($field = $meta->fetch_field()) {
            $params[] = &$row[$field->name];
        }
        call_user_func_array([$this->query, 'bind_result'], $params);
        $result = [];
        while ($this->query->fetch()) {
            if ('all' == $type) {
                $r = [];
                foreach ($row as $key => $val) {
                    $r[$key] = $val;
                }
                $result[] = $r;
            } else {
                foreach ($row as $key => $val) {
                    $result[$key] = $val;
                }
            }
        }
        $this->query->close();
        return $result;
    }

    /**
     * Return number of rows
     *
     * @return mixed
     */
    public function numRows()
    {
        $this->query->store_result();
        return $this->query->num_rows;
    }

    /**
     * Close database connection
     *
     * @return bool
     */
    public function close()
    {
        return $this->connection->close();
    }

    /**
     * Return the number of affected rows
     *
     * @return mixed
     */
    public function affectedRows()
    {
        return $this->query->affected_rows;
    }

    /**
     * Get variable type
     *
     * @param $var
     *
     * @return string
     */
    private function _getType($var)
    {
        if (is_string($var)) {
            return 's';
        }
        if (is_float($var)) {
            return 'd';
        }
        if (is_int($var)) {
            return 'i';
        }
        return 'b';
    }
}
