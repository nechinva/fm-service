<?php

/**
 * Core Api class
 */
abstract class Api
{
    /**
     * @var string - Api version
     */
    public $apiVersion = 'v1';

    /**
     * @var array - Requested uri
     */
    public $requestUri = [];

    /**
     * @var array - Requested parameters
     */
    public $requestParams = [];

    /**
     * @var array - Parsed requested uri
     */
    public $parsedUri = [];

    /**
     * @var string - Requested method (GET|POST|PUT|DELETE)
     */
    protected $method = '';

    /**
     * @var string - Requested action
     */
    protected $action = '';

    /**
     * @var array - Available actions
     */
    protected $availableActions = ['index', 'view', 'create', 'update', 'delete'];

    /**
     * Api constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: *");
        header("Access-Control-Allow-Headers: *");
        header("Content-Type: application/json");

        $this->requestUri    = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
        $this->requestParams = $_REQUEST;

        $this->parseUri();

        // Define request method
        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else {
                if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                    $this->method = 'PUT';
                } else {
                    throw new Exception("Unexpected header");
                }
            }
        }
    }

    /**
     * Parse requested uri
     * [domain]/api/items/version/action/id/
     */
    private function parseUri()
    {
        $this->parsedUri['api']     = array_shift($this->requestUri);
        $this->parsedUri['version'] = array_shift($this->requestUri);
        $this->parsedUri['entity']  = array_shift($this->requestUri);
        $this->parsedUri['action']  = array_shift($this->requestUri);
        $this->parsedUri['id']      = array_shift($this->requestUri);
    }

    /**
     * Get the item id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->parsedUri['id'] ?? null;
    }

    /**
     * Get request parameter
     *
     * @param $name
     * @param $default
     *
     * @return mixed
     */
    public function getParameter($name, $default)
    {
        return $this->requestParams[$name] ?? $default;
    }

    /**
     * Fetch all HTTP request headers
     *
     * @return array|false
     */
    public function getAllHeaders()
    {
        // If you using nginx instead of apache
        if (!function_exists('getallheaders')) {
            function getallheaders()
            {
                $headers = [];
                foreach ($_SERVER as $name => $value) {
                    if (substr($name, 0, 5) == 'HTTP_') {
                        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                    }
                }
                return $headers;
            }
        }

        return getallheaders();
    }

    /**
     * Return the response
     *
     * @param     $data
     * @param int $status
     *
     * @return false|string
     */
    protected function response($data, $status = 500)
    {
        header("HTTP/1.1 " . $status . " " . $this->requestStatus($status));
        return json_encode($data);
    }

    /**
     * Return the requested status
     *
     * @param $code
     *
     * @return mixed
     */
    private function requestStatus($code)
    {
        $status = [
            200 => 'OK',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        ];
        return ($status[$code]) ? $status[$code] : $status[500];
    }

}