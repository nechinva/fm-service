<?php

/**
 * Class ItemsApi
 */
class ItemsApi extends EntityApi
{
    /**
     * Get all items (no id) or one item by item id
     *
     * URI: [domain]/api/v1/items/index/{itemId}
     * Method: GET
     *
     * @return false|string
     * @throws Exception
     */
    public function indexAction()
    {
        $itemId = $this->getId();
        if ($itemId) {
            $items = (new DB())->query('SELECT `id`, `list_id`, `name` FROM `items` WHERE `id` = ? AND `active` = 1', $itemId)
                ->fetch();
        } else {
            $items = (new DB())->query('SELECT `id`, `list_id`, `name` FROM `items` WHERE `active` = 1')->fetch();
        }
        if ($items) {
            return $this->response(['data' => $items], 200);
        }

        return $this->response(['error' => 'Data not found'], 404);
    }

    /**
     * Get items for selected list
     *
     * URI: [domain]/api/v1/items/view/{listId}/?last=1
     * Method: GET
     *
     * @return false|string
     * @throws Exception
     */
    public function viewAction()
    {
        // List id must be the first parameter after /view/x
        $listId = $this->getId();
        if ($listId) {
            $limit = $this->getParameter('last', 0) ? ' ORDER BY id DESC LIMIT 1' : '';
            $items = (new DB())->query('SELECT `id`, `list_id`, `name` FROM `items` WHERE `list_id` = ? AND `active` = 1' . $limit, $listId)->fetch();
            if ($items) {
                return $this->response(['data' => $items], 200);
            }
        }

        return $this->response(['error' => 'Data not found'], 404);
    }

    /**
     * Create an item for selected list
     *
     * URI: [domain]/api/v1/items/create/{listId}/?name=item-name
     * Method: POST
     *
     * @return false|string
     * @throws Exception
     */
    public function createAction()
    {
        // List id must be the first parameter after /create/x
        $listId   = $this->getId();
        $itemName = $this->getParameter('name', '');
        if ($listId && '' !== trim($itemName)) {
            $insert = (new Db())->query('INSERT INTO `items` (`list_id`, `name`) VALUES (?,?)', $listId, trim($itemName));
            if ($insert->affectedRows()) {
                return $this->response(['data' => $insert->affectedRows()], 200);
            }
        }

        return $this->response(['error' => 'Saving error'], 500);
    }

    /**
     * Update an item for selected list
     *
     * URI: [domain]/api/v1/items/update/{itemId}/?name=new-item-name
     * Method: PUT
     *
     * @return false|string
     * @throws Exception
     */
    public function updateAction()
    {
        // Item id must be the first parameter after /update/x
        $itemId   = $this->getId();
        $itemName = $this->getParameter('name', '');
        if ($itemId && '' !== $itemName) {
            $update = (new Db())->query('UPDATE `items` SET `name` = ? WHERE `id` = ?', trim($itemName), $itemId);
            if ($update->affectedRows()) {
                return $this->response(['data' => $update->affectedRows()], 200);
            }
        }

        return $this->response(['error' => 'Updating error'], 400);
    }

    /**
     * Delete an item for selected list
     *
     * URI: [domain]/api/v1/items/delete/2/
     * Method: DELETE
     *
     * @return false|string
     * @throws Exception
     */
    public function deleteAction()
    {
        // Item id must be the first parameter after /delete/x
        $itemId = $this->getId();
        if ($itemId) {
            $delete = (new Db())->query('UPDATE `items` SET `active` = 0 WHERE `id` = ?', $itemId);
            if ($delete->affectedRows()) {
                return $this->response(['data' => $delete->affectedRows()], 200);
            }
        }

        return $this->response(['error' => 'Deleting error'], 500);
    }
}